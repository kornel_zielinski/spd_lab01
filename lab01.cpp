#include <iostream>
#include <string>
#include <vector>
#include <fstream>

class Task
{
public:
	int r, p, q, id;
  Task(int R, int P, int Q, int ID)	{r=R; p=P; q=Q; id=ID;}
  Task(){r=0; p=0; q=0; id=0;}
  void swap(Task&);
};

//wieksza z dwoch wartosci
int max(int a, int b); 

//wczytanie danych z pliku
void load_data(const char* filename, std::vector<Task> &tasks);

//wypisanie kontrolne kolejnych zadan na wyjscie
void print_tasks(std::vector<Task> list);

//wypisanie kolejcnosci zadan
void print_order(std::vector<Task> list);

//funkcja mierzaca czas wykonania zestawu danych
int time(std::vector<Task> &list);

//funkcja sortowania wylacznie po R
void sortR (std::vector<Task> &list);

//sortowanie po R a nastÍpnie kolejne wykonywanie zadan wedlug priorytetu - Q
void sortRQ (std::vector<Task> &list);

// Doslownie magia, to nie ma prawa dzialac tak dobrze jak dziala.
// Jest to funkcja luzno oparta na sortowaniu babelkowym, jednak zamiast sortowac
// wedlug danego klucza po kazdym przestawieniu sprawdza czy calkowity czas 
// ulegl poprawie. Jesli nie to zamienia elementy z powrotem, zastosowanie tej 
// funkcji moze wiec wylacznie poprawic sytuacje. Sama w sobie nie dziala za 
// dobrze, lecz w polaczeniu z normalna funkcja sortujaca pozwala na znaczna 
// poprawe czasu dzialania. Zeby bylo smieszniej, uzywamy tez wariantu tej
// funkcji, ktory zamienia nie dwa kolejne elementy, a elementy oddalone o n
// gdyz uzycie tej funkcji z roznymi wartosciami n poteguje jej dzialanie. 
// Zlozonosc obliczeniowa prawdopodobnie nieprzyzwoicie wysoka, ale i tak 
// zdecydowanie mniejsza niz O(n!)

void bubble (std::vector<Task> &list, int n);


int main(int argc, char** argv)
{
	std::vector<Task> tasks;
  
  if(argc < 2)
  {
	  std::cout << "Uzycie: " << argv[0] << " dane.txt {wiecej_danych.txt}\n";
    return 0;
  }

  int time_total=0, time_R, time_RQ, time_data;
	for(int i=1; i<argc; i++)
  {
	  load_data(argv[i], tasks);
    std::cout << argv[i] << '\n';

	  sortR(tasks);
    time_R = time(tasks);

		sortRQ(tasks);
    time_RQ = time(tasks);

    if(time_R < time_RQ)
		{
		  sortR(tasks);
      time_data = time(tasks);
    }
		else
		  time_data = time_RQ;

    for(int k = 1; k < tasks.size(); k++)
      bubble(tasks, k);
    time_data = time(tasks);
    
		print_order(tasks);
	  std::cout << std::endl << "czas: " << time_data << "\n\n";
    time_total = time_total + time_data;
  }

  std::cout << "Calkowity czas: "<< time_total << '\n';
	return 0;
}

void Task::swap(Task &that)
{
  Task temp;
  temp = *this;
  *this = that;
  that = temp;	
}

int max(int a, int b)
{
  if(a>=b)
    return a;
  else
    return b;	
}

void load_data(const char* filename, std::vector<Task> &tasks)
{
	int count, r, p, q;
  Task* temp_task;
  
	std::ifstream data;
  tasks.clear();
	data.open(filename);
  if(!data.is_open())
  {
	  std::cerr << "bledna nazwa pliku\n";
		return;
  }

	data >> count;
  for(int i=1; i<=count; i++)
  {	
    data >> r >> p >> q;
		temp_task = new Task(r, p, q, i);
    tasks.push_back(*temp_task);
		delete temp_task;
  }
	
  if(tasks.size() != count)
		std::cerr << "blad w odczycie z pliku\n";
}

void print_tasks(std::vector<Task> list)
{
	for(int i=0; i<list.size(); i++)
		std::cout << list[i].r << ' ' << list[i].p << ' ' <<list[i].q << '\n';
}

void print_order(std::vector<Task> list)
{
	for(int i=0; i<list.size(); i++)
		std::cout << list[i].id << ' ';
  std::cout << std::endl;
}

int time(std::vector<Task> &list)
{
	int t=0, tq=0;
  for(int i=0; i<list.size();i++)
  {
	  if(list[i].r > t)
	    t=list[i].r;
    t=t+list[i].p;
    if(t+list[i].q > tq)
      tq=t+list[i].q;
  }
	return tq;
}

//tak, to bubblesort
void sortR (std::vector<Task> &list)
{
	for(int i=1; i < list.size(); i++)
  {
	  for(int j=0; j < list.size() - i; j++)
    {
	    if(list[j].r>list[j+1].r)
        list[j].swap(list[j+1]);
		}
  }

  for(int k = 1; k < list.size(); k++)
    bubble(list, k);
}

void sortRQ (std::vector<Task> &list)
{
	int t=0, priority;
  std::vector<Task>::iterator pos;  
  bool task_ready = 0;
 
  //kopiujemy list na queue, jednoczesnie sortujac przez wstawianie
  std::vector<Task> queue;                           
  for(int i = 0; i<list.size(); i++)
  {
	  std::vector<Task>::iterator j;
	  for(j = queue.begin(); j<queue.end(); j++)
    {
	    if(list[i].r < j->r)
        break;
    }
    queue.insert(j, list[i]);
  }
  
  //oprozniamy list, na ktore bedziemy kopiowac wykonywane zadania
  list.clear();

  while(!queue.empty())
  {
		priority = -1;
    //z dostepnych w danej chwili t zadan wybieramy to o najwiekszym q
	  for(std::vector<Task>::iterator i = queue.begin(); i->r <= t && i < queue.end(); i++)
	  {
		  if(i->q > priority)
	    {
		    pos = i;
	      priority = i->q;
	    }
      task_ready = true;
	  }
    //wykonujemy wytypowane zadanie, jesli jakiej jest
    if(task_ready)
    {
		  t = t + pos->p;
	    list.push_back(*pos);
	    queue.erase(pos);
	    task_ready = false;
   }
   else
   //jezeli w danej chwili nie ma zadan to bierzemy pierwsze dostepne
     t = queue[0].r;        
	}
  for(int k = 1; k < list.size(); k++)
    bubble(list, k);
}


void bubble (std::vector<Task> &list, int n)
{
	int old_time, new_time;
	bool swapped=true;
  old_time = time(list);
  while(swapped)
  {
	  swapped = false;
	  for(int j=0; j < list.size()-n; j++)
    {
    	list[j].swap(list[j+n]);
      new_time = time(list);
	    if(new_time >= old_time)
        list[j].swap(list[j+n]);
      else
        {
	        swapped = true;
          old_time=new_time;
        }
		}
  }
}

